const path = require('path');

const { cleanName, version } = require('../utils/get-module-name')()

module.exports = {
  [cleanName]: {
    version,
    name: cleanName,
  }
};
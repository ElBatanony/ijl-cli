const defaultConfig = require("./config")

module.exports = {
  baseUrl: defaultConfig.baseUrl,
  pageTitle: "Loading ...",
  apps: require("./app"),
  navigations: require("./navigations"),
  config: defaultConfig,
  fireappVersion: '1.0.0'
};
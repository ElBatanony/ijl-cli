const express = require("express");
const webpack = require("webpack");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const webpackDevMiddleware = require("webpack-dev-middleware");
const webpackHotMiddleware = require("webpack-hot-middleware");
const openBrowser = require("react-dev-utils/openBrowser");
const { applyHbs } = require("@ijl/templates");

const getProjectConfig = require("./utils/get-project-config");
const { entryPoint, cleanName: name } = require("./utils/get-module-name")();
process.env.ENTRY = entryPoint;
const getModuleApi = require("./utils/get-module-api");
const getConfig = require("./utils/get-config-template");

const getmoduleMiddleware = require("../../middleware/get-module");

const {
  apiPath = "stubs/api",
  config = {},
  navigations = {},
  apps = {}
} = getProjectConfig();

module.exports = ({ port, withOpenBrowser }) => {
  const app = express();
  applyHbs(app);

  app.use(session({
    secret: 'LONG_RANDOM_STRING_HERE',
    resave: true,
    saveUninitialized: false
  }));
  app.use(cookieParser());

  const appPath = `/${name}`;

  // hot reload -->
  const webpackConfig = require("../webpack/development");
  const compiler = webpack(webpackConfig);

  app.use(
    webpackDevMiddleware(compiler, {
      publicPath: webpackConfig.output.publicPath
    })
  );
  
  app.use(webpackHotMiddleware(compiler));
  // <-- hot reload

  app.get("/", (req, res) => res.redirect(appPath));

  getModuleApi(app, apiPath);

  const envConfig = getConfig({ config, navigations, apps });
  app.use(appPath, function(request, response) {
    response.render("dev.hbs", envConfig);
  });

  app.use(
    envConfig.baseUrl,
    express.Router().get(/\/([.-\w]+)\/([.-\w\d]+)\/(.*)/, getmoduleMiddleware)
  );
  const PORT = process.env.PORT;
  app.listen(PORT, () => {
    const openUrl = `http://localhost:${PORT}${appPath}`;
    console.log("🛠", "Open:", openUrl);

    if (withOpenBrowser) {
      openBrowser(openUrl);
    }
  });
};
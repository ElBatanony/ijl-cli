const path = require('path');

module.exports = () => {
    const package = require(path.resolve('package'))
    return {
        name: package.name,
        cleanName: package.name,
        version: package.version,
        entryPoint: package.main
    }
}

const path = require('path');

module.exports = (app, location) => {
    app.use('/api', require(path.resolve(location)))
}

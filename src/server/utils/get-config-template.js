const {
  apps: appsBase,
  config: configBase,
  navigations: navigationsBase,
  ...rest
} = require("../templates");

module.exports = ({ apps, config, navigations }) => {
  return {
    apps: { ...appsBase, ...apps },
    config: { ...configBase, ...config },
    navigations: { ...navigationsBase, ...navigations },
    ...rest
  };
};

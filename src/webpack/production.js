const merge = require('webpack-merge');
const path = require('path');

const base = require('./base');

module.exports = merge(base, {
  mode: 'production',
  entry: {
    index: path.resolve(process.env.ENTRY)
  }
});

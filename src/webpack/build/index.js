const webpack = require("webpack");
const path = require("path");

module.exports = () => {
  webpack(
    require(path.resolve(__dirname, "../webpack.config.js")),
    (error, stats) => {
      if (error) {
        throw error;
      }
      console.log(
        `Output:\n${stats.toString({
          chunks: false
        })}`
      );
    }
  );
};

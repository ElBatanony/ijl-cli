const webpack = require("webpack");
const path = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const { TsConfigPathsPlugin } = require("awesome-typescript-loader");
const webpackCopy = require("copy-webpack-plugin");
const merge = require("webpack-merge");

const getProjectConfig = require("../../server/utils/get-project-config");

const { webpackConfig = {} } = getProjectConfig();

const outputDirectory = "dist";

const cwd = process.cwd();

module.exports = merge.strategy(
  {
    module: 'replace',
  })(
  {
    output: {
      filename: "[name].js",
      path: path.resolve(cwd, outputDirectory),
      libraryTarget: "umd",
      globalObject: `(typeof self !== 'undefined' ? self : this)`
    },
    plugins: [
      new CleanWebpackPlugin(),
      new webpackCopy([{ from: "./static/", to: "static" }])
    ],
    resolve: {
      modules: [cwd, "node_modules"],
      extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".css"],
      plugins: [new TsConfigPathsPlugin()]
    },

    module: {
      rules: [
        { parser: { system: false } },
        {
          test: /\.tsx?$/,
          loader: "awesome-typescript-loader"
        },
        {
          test: /\.(jpe?g|gif|png|svg|woff|ttf|eot|wav|mp3)$/,
          loader: "file-loader"
        }
      ]
    },
    // externals: {
    //   react: "react",
    //   "react-dom": "react-dom",
    //   redux: "redux",
    //   "react-redux": "react-redux",
    //   "styled-components": "styled-components"
    // }
  },
  webpackConfig
);

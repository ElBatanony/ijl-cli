const moduleData = require('../server/utils/get-module-name')();

if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = "production";
}
if (!process.env.ENTRY) {
  process.env.ENTRY = moduleData.entryPoint;
}

if (process.env.NODE_ENV === "development") {
  module.exports = require("./development");
} else if (process.env.NODE_ENV === "production") {
  module.exports = require("./production");
} else {
  throw new Error(
    "Необходимо указать корректный NODE_ENV"
  );
}

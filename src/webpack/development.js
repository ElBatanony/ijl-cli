const base = require("./base");
const merge = require("webpack-merge");

const webpack = require("webpack");
const path = require("path");

const outputDirectory = "dist";

module.exports = merge(
  {
    mode: "development",
    entry: {
      index: [
        path.resolve(process.env.ENTRY)
      ]
    },
    output: {
      filename: "[name].js",
      path: path.resolve(process.cwd(), outputDirectory),
      libraryTarget: "umd",
      globalObject: `(typeof self !== 'undefined' ? self : this)`
    },
    node: {
      fs: "empty"
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.DefinePlugin({
        "typeof window": JSON.stringify("object")
      })
    ],
    devtool: "source-map"
  },

  base
);

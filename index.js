const {
  getConfig,
  getConfigValue,
  getNavigations,
  getNavigationsValue
} = System.get("root.scope");

module.exports = { getConfig, getConfigValue, getNavigations, getNavigationsValue };

const getModuleUrl = require('../src/server/utils/get-module-url');

module.exports = (req, res) => {
    let path

    if(req.params[0] === 'fire.app') {
        path = getModuleUrl('@ijl/fire.app', req.params[1], req.params[2]);
    } else if (req.params[0] === 'uds-ui') {
        path = getModuleUrl('@ijl/uds-ui/dist', req.params[1], req.params[2]);
    } else {
        path = getModuleUrl(req.params[0], req.params[1], req.params[2]);
    }

    if (path) {
        res.sendFile(path);
    } else {
        res.status(404).send();
    }
}
